using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    
    public static GameManager instance;
    private int monsters = 0;
    public float delayBetweenSpawn = 3f;
    public float minDelayBetweenSpawn = 0.5f;
    public float timeOfChangeDifficulty = 10f;
    public int maxMonsters;
    public float probabiltyOfSpawnPowerUp = 0.3f;
    public GameObject gameOverPanel;
    public GameObject newRecordPanel;
    public InputField highScoreInput;
    public GameObject upperPanel;
    public GameObject lowerPanel;
    public GameObject losePanel;
    public Text playerNameText;
    public Text recordScoresText;
    public List<GameObject> monstersPrefabs = new List<GameObject>();
    public List<GameObject> powerUpPrefabs = new List<GameObject>();

    private List<GameObject> createdMonsters = new List<GameObject>();
    

    private Text monstersCount;
    private Text defeatedMonstersCount;
    private Text scores;
    private int defeatedMonstersNumber = 0;
    private int scoresNumber = 0;
    private int difficulty = 0;

    private Transform parentOfMonsters;
    private Text infoText;
    private Text gameTimeText;

    private float gameTime = 0;
    private float lowerEdge;
    private float upperEdge;
    private float leftEdge;
    private float rightEdge;

    private float delay;
    private float distance = 2f;
    private float lastSpawn;

    private int numberOfRecord;
    private bool gameOver = false;

    private void Awake() {
        if (GameManager.instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;

        lowerEdge = GameObject.Find("Lower_Edge").transform.position.z;
        upperEdge = GameObject.Find("Upper_Edge").transform.position.z;
        leftEdge = GameObject.Find("Left_Edge").transform.position.x;
        rightEdge = GameObject.Find("Right_Edge").transform.position.x;

        parentOfMonsters = GameObject.Find("Monsters").transform;

        monstersCount = GameObject.Find("MonstersCount").GetComponent<Text>();
        defeatedMonstersCount = GameObject.Find("DefeatedCount").GetComponent <Text>();
        infoText = GameObject.Find("InfoText").GetComponent<Text>();
        gameTimeText = GameObject.Find("GameTimeText").GetComponent<Text>();
        scores = GameObject.Find("Scores").GetComponent<Text>();
        monstersCount.text = defeatedMonstersCount.text = scores.text = 0.ToString();
        monsters = 0;

        ChangeMonstersCountText();
        ChangeInfoText();

        defeatedMonstersCount.text = "0";
        scores.text = "0";

        lastSpawn = Time.time;
        delay = delayBetweenSpawn;
    }

    private void Start() {
        Monster.boost = 0;
        //ShowHighScore();
    }

    void FixedUpdate() {
        if (!gameOver) {
            if (Time.time - lastSpawn > delay && monsters < maxMonsters) {
                SpawnMonster();
                lastSpawn = Time.time;
                delay = delayBetweenSpawn;
            }
            gameTime += Time.deltaTime;
            gameTimeText.text = gameTime.ToString("F1");
            if (gameTime >= timeOfChangeDifficulty) {
                ChangeDifficulty();
                timeOfChangeDifficulty += 10f;
            }
        }
    }

    private void ChangeDifficulty() {
        difficulty++;
        if (probabiltyOfSpawnPowerUp >= 0.05f)
            probabiltyOfSpawnPowerUp -= 0.05f;
        if (delayBetweenSpawn - 0.5f >= minDelayBetweenSpawn)
            delayBetweenSpawn -= 0.5f;
        Monster.changeDifficulty = true;
        ChangeInfoText();
    }

    private void ChangeInfoText() {
        infoText.text = "Difficulty: " + difficulty + "\n" +
                        "Booster: " + difficulty + "\n" +
                        "Time of game:";
    }

    public void FreezeSpawnMonsters(float time) {
        if (Timer.start) {
            delay += time;
        }  else {
            delay = time;
            lastSpawn = Time.time;
            Timer.start = true;
        }
        Timer.time += time;
    }

    public Vector3 GetRandomPoint() {
        return new Vector3(Random.Range(GameManager.instance.leftEdge + distance, GameManager.instance.rightEdge - distance), -0.5f, Random.Range(GameManager.instance.lowerEdge + distance, GameManager.instance.upperEdge - distance));
    }

    private Vector3 GetEmptyPoint() {
        Vector3 point = GetRandomPoint();
        for (int i = 0; i < createdMonsters.Count; i++) {
            if (point.x > createdMonsters[i].transform.position.x - distance && point.x < createdMonsters[i].transform.position.x + distance) {
                if (point.z > createdMonsters[i].transform.position.z - distance && point.z < createdMonsters[i].transform.position.z + distance) {
                    i = 0;
                    point = GetRandomPoint();
                }
            }
        }
        return point;
    }

    public void AddScores(int scores) {
        scoresNumber += scores;
        this.scores.text = scoresNumber.ToString();
    }

    private void ChangeMonstersCountText() {
        monstersCount.text = monsters.ToString();
        if (monsters < 5) 
            monstersCount.color = Color.green;
        else if (monsters >= 5 && monsters < 8) 
            monstersCount.color = Color.yellow;
        else if (monsters >= 8 && monsters <= 10)
            monstersCount.color = Color.red;
    }

    public bool CheckHighScore() {
        int score = int.Parse(scores.text);
        int number = -1;
        for (int i = 4; i >= 0; i--) {
            if (score > PlayerPrefs.GetInt("HIGHSCORE" + i)) {
                number = i;
            }
            else {
                break;
            }
        }
        if (number == -1)
            return false;
        for (int i = 4; i > number; i--) {
            //Debug.Log("HIGHSCORE i: " + i + ", i-1: " + (i - 1));
            PlayerPrefs.SetInt("HIGHSCORE" + i, PlayerPrefs.GetInt("HIGHSCORE" + (i - 1)));
            PlayerPrefs.SetString("HIGHSCORENAME" + i, PlayerPrefs.GetString("HIGHSCORENAME" + (i - 1)));
        }
        PlayerPrefs.SetInt("HIGHSCORE" + number, score);
        PlayerPrefs.SetString("HIGHSCORENAME" + number, "Player");
        numberOfRecord = number;
        return true;
    }

    public void UpdateRecordsName () {
        string name = highScoreInput.text;
        PlayerPrefs.SetString("HIGHSCORENAME" + numberOfRecord, name);
        highScoreInput.gameObject.SetActive(false);
        playerNameText.gameObject.SetActive(true);
        playerNameText.text = name;
    }

    private void GameEnd() {
        losePanel.SetActive(false);
        if (!CheckHighScore()) {
            gameOverPanel.SetActive(true);
        } else {
            newRecordPanel.SetActive(true);
            recordScoresText.text = scores.text + " scores";
        }
        Time.timeScale = 0f;
    }

    private void GameLosePanelShow() {
        gameOver = true;
        lowerPanel.SetActive(false);
        upperPanel.SetActive(false);
        losePanel.SetActive(true);
        Invoke("GameEnd", 3f);
    }

    public void NextButton() {
        newRecordPanel.SetActive(false);
        gameOverPanel.SetActive(true);
    }

    private void ShowHighScore() {
        for (int i = 4; i >= 0; i--) {
            Debug.Log(i + ". Name: " + PlayerPrefs.GetString("HIGHSCORENAME" + i) + ", scores: " + PlayerPrefs.GetInt("HIGHSCORE" + i));
        }
    }

    private void SpawnMonster() {
        createdMonsters.Add(Instantiate(monstersPrefabs[Random.Range(0, monstersPrefabs.Count)], GetEmptyPoint(), Quaternion.identity, parentOfMonsters));
        monsters++;
        ChangeMonstersCountText();
        if (monsters >= maxMonsters) {
            //GameEnd();
            GameLosePanelShow();
        }
    }

    private void SpawnPowerUp(Vector3 pos) {
        if (Random.Range(0f, 1f) < probabiltyOfSpawnPowerUp)
            Instantiate(powerUpPrefabs[Random.Range(0, powerUpPrefabs.Count)], new Vector3(pos.x, 1.5f, pos.z), Quaternion.identity);
    }

    public void DeleteMonster(GameObject monster) {
        SpawnPowerUp(monster.transform.position);
        createdMonsters.Remove(monster);
        monster.GetComponent<Monster>().PlayEffect();
        AddScores(monster.GetComponent<Monster>().scores);
        Destroy(monster);
        monsters--;
        ChangeMonstersCountText();
        defeatedMonstersNumber++;
        defeatedMonstersCount.text = defeatedMonstersNumber.ToString();
    }

    public void DestroyAllMonsters() {
        while (createdMonsters.Count > 0) {
            DeleteMonster(createdMonsters[0]);
        }
    }
    
}
