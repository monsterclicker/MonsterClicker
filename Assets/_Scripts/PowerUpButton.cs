using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpButton : MonoBehaviour {
    public static PowerUpButton instance;
    public List<PowerUpData> buttons = new List<PowerUpData>();

    private void Awake() {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }
}

[System.Serializable]
public class PowerUpData {
    public Button button;
    public Text count;
    public string Name;
}
