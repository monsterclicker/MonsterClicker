using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Monster : MonoBehaviour {

    public float maxHealth;
    public int scores;
    public float speed;
    public float distanceOfCheck;
    public GameObject deathEffect;
    public Image fillImage;
    public GameObject healthBar;
    public Slider slider;
    public Gradient healthGradient;

    public static bool changeDifficulty = false;

    public static float boost = 0;
    private float health;
    private Vector3 pointToFollow;
    private Collider collider;
    private RaycastHit hit;
    private Animator animator;

    private void Start() {
        collider = GetComponent<Collider>();
        animator = GetComponent<Animator>();

        health = maxHealth + boost;
        speed = speed + boost;
        slider.value = CalculateHealth();

        fillImage.color = healthGradient.Evaluate(1f);

        ChangePointToFollow();
    }
    void Update() {
        transform.position = Vector3.MoveTowards(transform.position, pointToFollow, speed * Time.deltaTime);
        if (transform.position == pointToFollow) {
            ChangePointToFollow();
        }
        
        if (Physics.BoxCast(collider.bounds.center, transform.localScale, transform.forward, transform.rotation, distanceOfCheck, LayerMask.GetMask("Blocking", "Monster"))) {
            ChangePointToFollow();
        }

        slider.value = CalculateHealth();

        if (health < maxHealth) {
            healthBar.SetActive(true);
        }

        healthBar.transform.LookAt(healthBar.transform.position + Camera.main.transform.forward);

        if (changeDifficulty) {
            ChangeDifficulty();
            changeDifficulty = false;
        }
    }

    private void ChangeDifficulty() {
        boost++;
    }

    private float CalculateHealth() {
        fillImage.color = healthGradient.Evaluate(slider.normalizedValue);
        return health / (maxHealth + boost);
    }

    private void ChangePointToFollow() {
        pointToFollow = GameManager.instance.GetRandomPoint();
        Vector3 direction = pointToFollow - transform.position;
        transform.rotation = Quaternion.LookRotation(direction);
    }

    public void PlayEffect() {
        Instantiate(deathEffect, new Vector3(transform.position.x, 1.5f, transform.position.z), Quaternion.identity);
    }

    private void OnMouseDown() {
        health--;
        animator.SetTrigger("Hit");
        if (health <= 0) {
            GameManager.instance.DeleteMonster(gameObject);
            //GameManager.instance.AddScores(scores);
            PlayEffect();
        }
    }

}
