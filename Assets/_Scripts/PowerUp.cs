using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

    public string nameOfPowerUp;
    public float timeOfExistace;

    private float timeOfCreation;

    private void Start() {
        timeOfCreation = Time.time;
    }

    private void Update() {
        if (Time.time - timeOfCreation > timeOfExistace) {
            Destroy(gameObject);
            Destroy(transform.parent.gameObject);
        }
    }

    private void OnMouseDown() {
        PowerUpPanel.instance.AddPowerUp(nameOfPowerUp);
        Destroy(gameObject);
        Destroy(transform.parent.gameObject);
    }

}
