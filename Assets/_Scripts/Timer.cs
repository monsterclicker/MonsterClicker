using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public static bool start = false;
    public GameObject panel;
    public Text txt;

    public static float time = 0f;
    private void Start() {
        panel.SetActive(false);
        txt.text = time.ToString("F1");
    }


    public void Update() {
        if (time > 0) {
            panel.SetActive(true);
            time -= Time.deltaTime;
            txt.text = time.ToString("F1");
            if (time <= 0) {
                panel.SetActive(false);
                start = false;
            }

        }
        
    }

}
