using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEffect : MonoBehaviour {
    private float timeOfExistance = 1f;

    private void Start() {
        Invoke("DestroyEffect", timeOfExistance);
    }

    private void DestroyEffect() {
        Destroy(gameObject);
    }
}
