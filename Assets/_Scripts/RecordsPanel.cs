using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordsPanel : MonoBehaviour {
    
    public List<Text> names = new List<Text>();
    public List<Text> scores = new List<Text>();


    public void UpdateRecords() {
        for (int i = 0; i < names.Count; i++) {
            names[i].text = PlayerPrefs.GetString("HIGHSCORENAME" + i);
            scores[i].text = PlayerPrefs.GetInt("HIGHSCORE" + i).ToString();
        }
    }

}
