using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PowerUpPanel : MonoBehaviour {

    public static PowerUpPanel instance;
    public GameObject pausePanel;
    private void Awake() {
        if (PowerUpPanel.instance != null) {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    private int FindPowerUpIndex(string name) {
        int index = 0;
        for (int i = 0; i < PowerUpButton.instance.buttons.Count; i++) {
            if (PowerUpButton.instance.buttons[i].Name == name) {
                index = i;
            }
        }
        return index;
    }

    private bool UsePowerUp(string name) {
        int index = FindPowerUpIndex(name);
        int count = int.Parse(PowerUpButton.instance.buttons[index].count.text);
        if (count == 0) {
            return false;
        }
        count -= 1;
        PowerUpButton.instance.buttons[index].count.text = count.ToString();
        if (count == 0) {
            Color c = PowerUpButton.instance.buttons[index].button.GetComponent<Image>().color;
            c.a = 0.31f;
            PowerUpButton.instance.buttons[index].button.GetComponent<Image>().color = c;
        }
        return true;
    }

    public void AddPowerUp(string name) {
        int index = FindPowerUpIndex(name);
        int count = int.Parse(PowerUpButton.instance.buttons[index].count.text);
        count += 1;
        
        Color c = PowerUpButton.instance.buttons[index].button.GetComponent<Image>().color;
        c.a = 1f;
        PowerUpButton.instance.buttons[index].button.GetComponent<Image>().color = c;
        PowerUpButton.instance.buttons[index].count.text = count.ToString();
    }

    public void SnowPowerUpBehavior() {
        if (UsePowerUp("Snow")) {
            GameManager.instance.FreezeSpawnMonsters(5f);
        }
    }

    public void BombPowerUpBehavior() {
        if (UsePowerUp("Bomb"))
            GameManager.instance.DestroyAllMonsters();
    }

    public void PauseButton() {
        pausePanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ContinueButton() {
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void QuitButton() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void NewGameButton() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("GameScene");
    }

}
