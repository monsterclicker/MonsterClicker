using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour  {

    public GameObject mainMenu;
    public GameObject recordsMenu;
    public GameObject creditsMenu;
    public GameObject quitMenu;
    
    public void NewGameButton() {
        SceneManager.LoadScene("GameScene");
    }

    public void RecordsButton() {
        mainMenu.SetActive(false);
        recordsMenu.SetActive(true);
    }

    public void CreditsButton() {
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
    }

    public void BackButton() {
        recordsMenu.SetActive(false);
        creditsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void QuitButton() {
        mainMenu.SetActive(false);
        quitMenu.SetActive(true);
    }

    public void YesButton() {
        Application.Quit();
    }

    public void NoButton() {
        quitMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

}
